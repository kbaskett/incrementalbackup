import os
import re
import string
import sys
import tempfile
import time

import sublime
import sublime_plugin

from .src.BackupHandler import BackupHandler, BackupNavigator, BackupSettings

try:
    import sublimelogging
    logger = sublimelogging.getLogger(__name__)
except ImportError:
    import logging
    logger = logging.getLogger(__name__)

CompareInInstalled = False

def plugin_loaded():
    global CompareInInstalled
    CompareInInstalled = 'CompareIn' in sys.modules.keys()

def proceed_with_backup(view):
    if view is None:
        return False

    name = view.file_name()

    if not name:
        return False
    elif name.startswith(tempfile.gettempdir()):
        return False
    elif view.is_scratch():
        return False
    elif view.is_read_only():
        return False

    for p in BackupSettings.backup_paths_for_view(view):
        if name.startswith(p):
            logger.info('%s is a backup file; skipping backup', name)
            return False

    # This is repeated in case the view no longer exists
    if view is None:
        return False

    return True

class IncrementalBackupListenerCommand(sublime_plugin.EventListener):

    def on_load_async(self, view):
        self.do_backup(view, "backup_on_load")

    def on_pre_save_async(self, view):
        self.do_backup(view, "backup_on_save")

    def on_pre_close(self, view):
        self.do_backup(view, "backup_on_close")

    def do_backup(self, view, event):
        """Check and do a backup to all backup locations."""
        if not proceed_with_backup(view):
            return

        contents = view.substr(sublime.Region(0, view.size()))

        BackupHandler.update_backup_time(view)
        handlers = BackupHandler.get_backup_handlers_for_view(view)
        for b in handlers:
            sublime.set_timeout(lambda: self.backup_and_cleanup(b, event, contents), 0)

    def backup_and_cleanup(self, backup_handler, event, contents):
        if getattr(backup_handler.settings, event):
            backup_handler.do_backup(contents)
        backup_handler.cleanup_files()


class IncrementalBackupCommand(sublime_plugin.TextCommand):
    """Performs a backup on the given view.

    This command can be called from other commands to perform a backup in
    response to another event, or it can be called alone to create an on-demand
    backup, optionally with a name.

    """
     
    ValidChars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    ValidChars = frozenset(ValidChars)

    def run(self, edit, request_name = False):
        """Calls the show method of the DocFinder assigned to the view."""
        if not proceed_with_backup(self.view):
            return

        contents = self.view.substr(sublime.Region(0, self.view.size()))
        BackupHandler.update_backup_time(self.view)
        handlers = BackupHandler.get_backup_handlers_for_view(self.view)

        if request_name:
            self.request_name(handlers, contents)
            return
        else:
            sublime.set_timeout_async(lambda: self.do_backup(handlers, contents), 0)

    def request_name(self, handlers, contents, string = ''):
        """Displays an input panel so the user can enter a name for the backup."""
        self.view.window().show_input_panel('Backup name',
                                            string,
                                            lambda string: self.name_on_done(string, handlers, contents),
                                            lambda string: None,
                                            lambda: None)

    def name_on_done(self, string, handlers, contents):
        """On_done handler for the name input panel.

        The name is checked to ensure it contains no invalid characters. If it 
        does, the input panel is redisplayed. Otherwise, do_backup is called.

        """
        for c in string:
            if c not in IncrementalBackupCommand.ValidChars:
                break
        else:
            logger.debug("valid string")
            sublime.set_timeout_async(lambda: self.do_backup(handlers, contents, string), 0)
            return

        sublime.status_message("Illegal characters in Backup name")
        self.request_name(contents, string)


    def do_backup(self, handlers, contents, name = None):
        """Check and do a backup to all backup locations."""
        for b in handlers:
            b.do_backup(contents, name)
            b.cleanup_files()

    def is_visible(self, request_name = False):
        return proceed_with_backup(self.view)


class BrowseBackupsCommand(sublime_plugin.TextCommand):

    def run(self, edit, diffs = False, replace = False, compare = False):
        if compare and not CompareInInstalled:
            return

        self.temp = None

        nav = BackupNavigator.get_navigator_for_view(self.view)
        current_contents = self.view.substr(
            sublime.Region(0, self.view.size()))
        row = current_row(self.view)
        
        file_list = sorted(nav.get_file_list(), reverse = True)
        if current_contents == file_list[0].file_contents:
            file_list = file_list[1:]
        display_list = [f.quick_panel_contents() for f in file_list]

        if not file_list:
            sublime.status_message('No previous backups exist for this file')

        on_select = lambda n: self.on_select(n, file_list, diffs, replace, compare, row)
        if diffs:
            on_highlight = lambda n: self.on_highlight(n, file_list, diffs, current_contents, row)
        else:
            on_highlight = lambda n: self.on_highlight(n, file_list, diffs, row = row)
        
        self.view.window().show_quick_panel(display_list, on_select, 0, 0, on_highlight)

    def on_select(self, selection, file_list, diffs = False, replace = False, 
                  compare = False, row = 0):
        if selection == -1:
            self.view.window().focus_view(self.view)
            if self.temp is not None:
                sublime.set_timeout_async(lambda: delete_temp(self.temp), 0)
        else:
            f = file_list[selection]
            if compare and CompareInInstalled:
                self.view.window().focus_view(self.view)
                args = {'left_file': self.view.file_name(),
                        'right_contents': f.file_contents,
                        'right_title': f.opened_view_name(),
                        'right_read_only': True}
                sublime.run_command('compare_in', args)
                if self.temp is not None:
                    sublime.set_timeout_async(lambda: delete_temp(self.temp), 0)
            elif replace:
                contents = f.file_contents
                self.view.run_command('replace_all', {'contents': contents})
                self.view.window().focus_view(self.view)
                if self.temp is not None:
                    sublime.set_timeout_async(lambda: delete_temp(self.temp), 0)
            else:
                content_view = self.display_temp(f, False, row = row)
                sublime.set_timeout_async(
                    lambda: self.post_open(content_view, f.opened_view_name()), 
                    0)

    def on_highlight(self, selection, file_list, diffs = False, 
                     current_contents = '', row = 0):
        if selection == -1:
            pass
        else:
            f = file_list[selection]
            if diffs:
                if selection == 0:
                    newer_contents = current_contents
                    newer_time = time.localtime()
                else:
                    newer = file_list[selection - 1]
                    newer_contents = newer.file_contents
                    newer_time = newer.datetime
            else:
                newer_contents = None
                newer_time = None
            self.display_temp(f, diffs, newer_contents, newer_time, row)

    def display_temp(self, backup_file, diffs, newer_contents = None, 
                     newer_time = None, row = 0):
        """Displays a transient view with the contents of the file.

        Keyword arguments:
        backup_file - The BackupFile object to display
        diffs - True to show a diff. False to show the full file contents.
        newer_contents - If diffs, the contents to diff with respect to. If
            this is the first backup, the current view contents. Otherwise,
            the contents of the previous view.
        newer_time - If diffs, a structured time representing the 
            modification time of the newer contents. If this is the first
            backup, the current datetime. Otherwise, the datetime of the
            previous backup.
        row - The row to show in the opened file.

        This function generates the contents to show, writes them to a temp 
        file, and opens that file. The previous temp file is deleted.

        """
        if diffs:
            contents = backup_file.get_diff_contents(newer_contents, newer_time)
            ext = '.diff'
        else:
            contents = backup_file.file_contents
            ext = os.path.splitext(self.view.file_name())[1]
        
        old_temp = self.temp
        with tempfile.NamedTemporaryFile(mode='w', suffix=ext, delete=False) as temp:
            temp.write(contents)
        self.temp = temp.name
        if diffs:
            name = self.temp
        else:
            name = '%s:%s:%s' % (self.temp, row + 1, 0)
        temp_view = self.view.window().open_file(name, sublime.TRANSIENT | sublime.ENCODED_POSITION)

        if old_temp is not None:
            sublime.set_timeout_async(lambda: delete_temp(old_temp), 0)

        return temp_view

    def post_open(self, view, name):
        """Sets view attributes after the selected file is opened.

        1. Sets scratch to True
        2. Sets the name of the view
        3. Deletes the temp file

        """
        while view.is_loading():
            time.sleep(0.01)

        view.set_scratch(True)
        view.set_name(name)

        sublime.set_timeout_async(lambda: delete_temp(self.temp), 0)

    def is_visible(self, diffs = False, replace = False, compare = False):
        """Return True to display the command or False to hide it.

        The command is hidden under the following conditions:
        1. It is a compare command and the compare package is not installed.
        2. It is a replace command and the view is read-only.
        3. The view does not represent a file.
        4. The view is a temp file.
        5. The view is a backup file.
        6. The view is a scratch file.

        """
        if compare and not CompareInInstalled:
            return False
        elif replace and self.view.is_read_only():
            return False

        name = self.view.file_name()
        if not name:
            return False
        elif name.startswith(tempfile.gettempdir()):
            return False
        elif self.view.is_scratch():
            return False

        for p in BackupSettings.backup_paths_for_view(self.view):
            if name.startswith(p):
                return False

        return True


def delete_temp(temp):
    """Deletes the file if it exists."""
    try:
        os.remove(temp)
    except FileNotFoundError:
        pass

def current_row(view):
    """Returns the row number for the first selection."""
    return view.rowcol(
        view.line(
            view.sel()[0]).begin()
        )[0]

class ReplaceAllCommand(sublime_plugin.TextCommand):
    """Replaces all the text in the current view with the given contents.

    The selection is restored to the current selection.

    """

    def run(self, edit, contents = ''):
        row, col = self.view.rowcol(
                        self.view.line(
                            self.view.sel()[0]).begin()
                   )
        reg = sublime.Region(0, self.view.size())
        self.view.replace(edit, reg, contents)
        self.view.sel().clear()
        point = self.view.text_point(row, col)
        reg = sublime.Region(point, point)
        self.view.sel().add(reg)
        self.view.show(reg, True)

