# IncrementalBackup

A [Sublime Text 3](http://www.sublimetext.com/) package for creating a copy of a file when it is opened, saved or closed.

## Features

*   Backup to multiple locations
*   Backup when a file is opened. closed or saved
*   Backup full copies of files or incremental diffs to save space
*   Purge your backups after a certain number or a certain time frame
*   Archive certain backups to prevent them from being purged
*   Create named backups to keep track of important revisions
*   Browse backups across multiple locations within sublime by viewing the entire file or the changes introduced in that version
*   Open a backup as a new file, replace the contents of a file with a backup, or compare the backup to the current version of the file using the CompareIn package
*   Create backups from other tools using the provided commands
*   Define settings at the package, backup location, or project levels.


# Installation

## Package Control

Install [Package Control](http://wbond.net/sublime_packages/package_control). Add this repository (https://bitbucket.org/kbaskett/incrementalbackup) to Package Control. IncrementalBackup will show up in the packages list.

## Manual installation

Go to the "Packages" directory (`Preferences` > `Browse Packages…`). Then download or clone this repository:

https://bitbucket.org/kbaskett/incrementalbackup.git


# Options

When editing settings, always make the changes to your user settings file (`Preferences` > `Package Settings` > `Incremental Backup` > `Settings - User`). Any changes to the package settings file will be overwritten when the package is updated.

In addition to the user settings, settings can also be specified in *.sublime-project folders. When doing so, they should be specified under settings > Incremental Backup. All of the following settings can be added there.

Any backup locations defined in the project are added to those defined for the package. Settings are evaluated with the following priorities, from highest to lowest:

*   Project location-specific settings
*   Package location-specific settings
*   Project default settings
*   Package default settings

```javascript
{
    // Any of the preferences defined here can also be defined in *.sublime-project files
    // to enable per-project settings.

    // Default preferences will be used when a location-specific preference is not provided.
    "default": {
        "backup_on_load": true,

        "backup_on_save": true,
        
        "backup_on_close": true,
        
        // If just a number, a full copy will be saved every n backups.
        // If d or days is after the number, a full copy will be saved every n days.
        // Reverse diffs will be saved on the other days.
        "save_full_copy_every": 5,

        // If just a number, only n backups will be saved
        // If d or days is after the number, backups older than n days will be purged
        "purge_after": 20,
        
        // If just a number, every nth backup will be archived instead of purged
        // If d or days is after the number, the last backup of the day will be archived instead of purged
        // Archives are always stored as complete files.
        "archive_every": "1 day",

        // If true, archived versions will be moved into an Archive subfolder
        "use_archive_folder": false
    },

    // Any number of backup locations can be specified here.
    // Any of the default preferences can also be specified for a backup location. 
    // If so, the backup location preference will override the default.
    "backup_locations": [
        // {
        //      "backup_path": "C:\\Incremental Backups",
        // }
    ]
}
```


# Usage

After configuring your settings, files will be copied to any defined backup locations according to the settings.

The following commands are available in the command palette:

*   Incremental Backup: Browse Backups
*   Incremental Backup: Browse Backups by Diff
*   Incremental Backup: Restore Backup
*   Incremental Backup: Restore Backup by Diff
*   Incremental Backup: Compare Backup
    Note that this command requires the CompareIn package
*   Incremental Backup: Compare Backup by Diff
    Note that this command requires the CompareIn package
*   Incremental Backup: Create Backup
*   Incremental Backup: Create Named Backup

# Credit
This package includes and uses [google-diff-match-patch](https://code.google.com/p/google-diff-match-patch/), released under the [Apache License](https://bitbucket.org/kbaskett/incrementalbackup/src/Release/src/diff_match_patch/COPYING), for creating, saving and restoring incremental diffs.

