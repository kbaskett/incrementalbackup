Incremental Backup Package Installation Instructions:

1. No backup locations are defined by default. In order for this plugin to work, you must define at 
   least one backup location. To do this, open Preferences > Package Settings > Incremental Backup >
   Settings - Default and Settings - User. The default settings file provides an example of how to
   define a backup location. All settings changes should be made in the User settings file to 
   prevent your settings being overwritten when the package is upgraded.

2. This package integrates with the CompareIn package to allow for comparing a backup to the current
   contents of the file. If you want this functionality, you need to install the CompareIn
   package as well.

3. This package does not include any keybindings by default. Feel free to define any you would like
   by going to Preferences > Package Settings > Incremental Backup > Key Bindings - User.

4. See https://bitbucket.org/kbaskett/incrementalbackup for more information.