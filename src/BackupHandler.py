from datetime import date
import difflib
import os
import re
import shutil
import time

import sublime

try:
    import sublimelogging
    logger = sublimelogging.getLogger(__name__)
except ImportError:
    import logging
    logger = logging.getLogger(__name__)

from .diff_match_patch import diff_match_patch


PACKAGE_NAME = 'IncrementalBackup'
FILE_TIME_FORMAT = "%Y.%m.%d %H.%M.%S"
DISPLAY_TIME_FORMAT = "%m/%d/%Y %H:%M:%S"
UNC_PREFIX = os.path.sep + os.path.sep
STRING_SPLITTER = re.compile(r'^.*?\n', re.MULTILINE)


def split_string_into_lines(string):
    """Splits the string into a list of lines while preserving linebreaks."""
    return STRING_SPLITTER.findall(string)


class BackupHandler(object):
    """Handles making backups to a specific folder."""

    Handlers = dict()

    @classmethod
    def get_backup_handlers_for_view(cls, view):
        """Returns a set of BackupHandler objects for the view.

        """
        handlers = []
        for p in BackupSettings.backup_paths_for_view(view):
            if os.path.isdir(os.path.dirname(p)):
                handlers.append(cls._get_handler_for_view(p, view))
        return handlers

    @classmethod
    def _get_handler_for_view(cls, backup_path, view):
        """Return an existing instance of the handler if one exists or create a new one."""
        try:
            h = BackupHandler.Handlers[(backup_path, view.file_name())]
        except KeyError:
            return BackupHandler(backup_path, view)
        else:
            h._reinitialize(view)
            return h

    @classmethod
    def update_backup_time(cls, view):
        """Update the time used to generate new file names."""
        BackupFile.update_backup_time()


    @property
    def file_backup_folder(self):
        if self._file_backup_folder is None:
            name = self.file_path
            drive, folder = os.path.splitdrive(name)
            if drive[-1] == ':':
                drive = drive[:-1]
            elif drive[:2] == UNC_PREFIX:
                drive = drive[2:]
            if folder[0] == os.path.sep:
                folder = folder[1:]
            self._file_backup_folder = os.path.join(self.backup_path, drive, folder)
        return self._file_backup_folder

    @property
    def next_version(self):
        return self._current_version + 1

    @property
    def files_iter(self):
        for f in self._files.copy():
            if os.path.isfile(f.backup_file_path):
                yield f
            else:
                self._files.remove(f)

    @property
    def archive_files_iter(self):
        for f in self._archive_files.copy():
            if os.path.isfile(f.backup_file_path):
                yield f
            else:
                self._archive_files.remove(f)

    @property
    def most_recent_backup(self):
        return self._most_recent_backup

    @property
    def most_recent_archive(self):
        return self._most_recent_archive


    def __init__(self, backup_path, view):
        """
        Attributes:
            file_path
            backup_path
            settings
            _file_backup_folder
            _current_version
            _files
            _archive_files
            _most_recent_backup
            _most_recent_archive

        """
        super(BackupHandler, self).__init__()
        self.file_path = view.file_name()
        self.backup_path = backup_path
        self._current_version = 0
        self._file_backup_folder = None
        self._files = []
        self._archive_files = []
        self._most_recent_backup = None
        self._most_recent_archive = None
        BackupHandler.Handlers[(backup_path, view.file_name())] = self
        self._reinitialize(view)
        if os.path.isdir(self.file_backup_folder):
            for f in os.listdir(self.file_backup_folder):
                full_path = os.path.join(self.file_backup_folder, f)
                if (f == 'Archive') and os.path.isdir(full_path):
                    for f in os.listdir(full_path):
                        try:
                            self.add_backup_file(os.path.join(full_path, f))
                        except ValueError:
                            pass
                else:
                    try:
                        self.add_backup_file(full_path)
                    except ValueError:
                        pass
        else:
            logger.info('Creating backup folder %s', self.file_backup_folder)
            os.makedirs(self.file_backup_folder)

    def _reinitialize(self, view):
        """Runs initializations needed every time a Handler is retrieved."""
        self.settings = BackupSettings(view, self.backup_path)
        if self.settings.use_archive_folder:
            archive_folder = os.path.join(self.file_backup_folder, 'Archive')
            if not os.path.isdir(archive_folder):
                os.makedirs(archive_folder)

    def __str__(self):
        return "BackupHandler: View = %s; Path: %s" % (self.view.id(), self.path)

    def file_backup_path(self, name = None):
        """Return the full path to the next backup file to write."""
        ext = os.path.splitext(self.file_path)[1]
        file_name = BackupFile.build_backup_file_name(self.next_version, ext, name)
        return os.path.join(self.file_backup_folder, file_name)

    def add_backup_file(self, file_path):
        """Adds a backup file to the list of files."""
        backup_file = BackupFile(file_path, self)
        self._files.insert(0, backup_file)
        self._files.sort(reverse = True)
        if backup_file.version > self._current_version:
            self._current_version = backup_file.version
        if ((self.most_recent_backup is None) or
            (self.most_recent_backup.datetime < backup_file.datetime)):
            self._most_recent_backup = backup_file
        if backup_file.is_archive:
            self._archive_files.insert(0, backup_file)
            self._archive_files.sort(reverse = True)
            if ((self.most_recent_archive is None) or
                (self.most_recent_archive.datetime < backup_file.datetime)):
                self._most_recent_archive = backup_file

    def get_file_list(self, include_archive = False):
        """Return a list of files in the current backup path."""
        file_list = []
        for f in self.files_iter:
            if (not include_archive) and f.is_archive:
                break
            file_list.append(f)
        return file_list

    def get_archive_file_list(self):
        """Returns a list of archive files."""
        return _archive_files.copy()

    def do_backup(self, contents, name = ''):
        """Backup the file to the path.

        Return True if the file was backed up; False otherwise.

        """
        # Check to see if the base of the path exists. If not, don't do the backup.
        if not os.path.isdir(self.file_backup_folder):
            logger.warning('Backup Path %s does not exist; skipping backup', self.backup_path)
            return False

        if not name and self.compare_to_most_recent_backup(contents):
            logger.info('%s is the same as the most recent backup; skipping backup', self.file_path)
            return True

        return self.make_backup(contents, name)

    def make_backup(self, contents, name = ''):
        """Copies a file to the backup location specified by this handler.

        Converts the previous backup to a diff according to the settings.

        """
        backup_file = self.file_backup_path(name)

        try:
            with open(backup_file, 'w') as f:
                f.write(contents)
        except OSError:
            logger.error("%s could not be backed up to %s", self.file_path, self.backup_path)
            return False
        else:
            logger.info("%s backed up to %s", self.file_path, self.backup_path)
            if ((self.most_recent_backup is not None) and
                not self.most_recent_backup.version_name):
                fc_quant, fc_unit = self.settings.parse_backup_increment('save_full_copy_every')
                if fc_quant <= 0:
                    pass
                elif fc_unit == 'daily':
                    if len(self._files) >= 2:
                        previous_file = self._files[1]
                        from_date = previous_file.date_object
                        logger.debug('from_date = %s', from_date)
                        to_date = self.most_recent_backup.date_object
                        logger.debug('to_date = %s', to_date)
                        elapsed = to_date - from_date
                        logger.debug('elapsed = %s', elapsed)
                        if elapsed.days < fc_quant:
                            self.most_recent_backup.convert_to_diff(contents)
                elif (self.most_recent_backup.version % fc_quant) != 0:
                    self.most_recent_backup.convert_to_diff(contents)
            self.add_backup_file(backup_file)

        return True

    def cleanup_files(self):
        """Purges and archives old backups."""
        pa_quant, pa_unit = self.settings.parse_backup_increment('purge_after')
        if pa_quant <= 0:
            return

        files_to_purge = []

        if pa_unit == 'daily':
            current_date = date.today()
            # logger.debug('current_date = %s', current_date)
            for f in self.get_file_list():
                elapsed = current_date - f.date_object
                # logger.debug('elapsed = %s', elapsed)
                if ((elapsed.days < pa_quant) or f.is_archive):
                    pass
                else:
                    files_to_purge.append(f)
        else:
            try:
                file_list = self.get_file_list()
                files_to_purge.extend(file_list[pa_quant:])
            except IndexError:
                pass

        # logger.debug('files_to_purge = %s', files_to_purge)
        if not files_to_purge:
            return

        ae_quant, ae_unit = self.settings.parse_backup_increment('archive_every')
        if ae_quant <= 0:
            [f.purge() for f in files_to_purge]
        elif ae_unit == 'daily':
            if self.most_recent_archive is None:
                recent_archive = min(files_to_purge)
                recent_archive.archive()
                files_to_purge.remove(recent_archive)
                self._most_recent_archive = recent_archive

            # logger.debug('files_to_purge = %s', files_to_purge)
            for f in sorted(files_to_purge):
                if f.version_name:
                    self.archive_file(f)
                    continue
                # logger.debug('self.most_recent_archive.date_object = %s', self.most_recent_archive.date_object)
                from_date = self.most_recent_archive.date_object
                # logger.debug('from_date = %s', from_date)
                to_date = f.date_object
                # logger.debug('to_date = %s', to_date)
                elapsed = to_date - from_date
                # logger.debug('elapsed = %s', elapsed)
                if elapsed.days < ae_quant:
                    self.purge_file(f)
                else:
                    self.archive_file(f)
        else:
            for f in files_to_purge[:]:
                if f.version_name:
                    self.archive_file(f)
                elif (f.version % ae_quant) == 0:
                    self.archive_file(f)
                else:
                    self.purge_file(f)
        self._files.sort(reverse = True)
        self._archive_files.sort(reverse = True)

    def archive_file(self, backup_file):
        """Archive the file and add it to the archived file list."""
        backup_file.archive()
        self._archive_files.insert(0, backup_file)
        if backup_file.date_object > self.most_recent_archive.date_object:
            self._most_recent_archive = backup_file

    def purge_file(self, backup_file):
        """Purge the file and remove it from the file lists."""
        backup_file.purge()
        self._files.remove(backup_file)
        if backup_file.is_archive:
            self._archive_files.remove(backup_file)
        if self.most_recent_backup is backup_file:
            self._files.sort(reverse = True)
            self._most_recent_backup = self._files[0]

    def compare_to_most_recent_backup(self, contents):
        """Return True if the current view is the same as the recently backed up file; False otherwise."""
        if self.most_recent_backup is None:
            return False
        else:
            return contents == self.most_recent_backup.get_file_contents()


class BackupNavigator(object):
    """Object that handles navigating backups between multiple backup locations."""

    Navigators = dict()

    def __init__(self, view):
        super(BackupNavigator, self).__init__()
        self.view = view
        BackupNavigator.Navigators[view.id()] = self
        self.reinitialize()

    def reinitialize(self):
        """Run initializations that need to be done every time a file is retrieved."""
        self.handlers = BackupHandler.get_backup_handlers_for_view(self.view)

    def get_file_list(self):
        """Return a list of unique backups over multiple backup locations."""
        file_dict = dict()

        for h in self.handlers:
            for f in h.get_file_list(include_archive = True):
                file_dict[f.datetime] = f

        return [file_dict[t] for t in sorted(file_dict.keys())]

    @classmethod
    def get_navigator_for_view(cls, view):
        """Return a new or existing instance of the object for the given view."""
        try:
            h = BackupNavigator.Navigators[view.id()]
            h.reinitialize()
            return h
        except KeyError:
            return BackupNavigator(view)


class BackupSettings(object):
    """Represents a settings object for a specific backup path.

    These settings objects are intended to be used during a command, but not
    saved for an extended period. They do not update as settings are updated.
    However, compiling the settings is extensive, so they should not be
    generated repeatedly.

    """

    Defaults = {"backup_on_load": True,
                "backup_on_save": True,
                "backup_on_close": True,
                "save_full_copy_every": 1,
                "purge_after": 0,
                "archive_every": 0,
                "use_archive_folder": False}

    BACKUP_INCREMENT_EXP = re.compile(r"(\d+) *(d(ays?)?)?")


    @classmethod
    def PackageSettings(cls):
        """Return the package settings object."""
        try:
            return cls._PackageSettings
        except AttributeError:
            cls._PackageSettings = sublime.load_settings(PACKAGE_NAME + '.sublime-settings')
            return cls._PackageSettings

    @classmethod
    def ProjectSettings(cls, view):
        """Return the project settings associated with a view."""
        try:
            project_data = view.window().project_data()
        except AttributeError:
            return None

        try:
            p_settings = project_data['settings']
        except KeyError:
            return None
        except TypeError:
            return None

        try:
            return p_settings[PACKAGE_NAME]
        except KeyError:
            return None

    @classmethod
    def backup_paths_for_view(cls, view):
        """Returns a list of backup paths defined in the current package and project settings."""
        backup_paths = set()
        for b in cls.PackageSettings().get('backup_locations', []):
            try:
                backup_paths.add(b['backup_path'])
            except KeyError:
                continue

        p_settings = cls.ProjectSettings(view)
        if p_settings is not None:
            try:
                project_locations = p_settings['backup_locations']
            except KeyError:
                project_locations = []

            for b in project_locations:
                try:
                    backup_paths.add(b['backup_path'])
                except KeyError:
                    continue
        return backup_paths


    @property
    def package_settings(self):
        """Return the package settings object."""
        return self.__class__.PackageSettings()

    @property
    def project_settings(self):
        """Return the the project settings at the time the BackupSettings object was initialized."""
        return self._project_settings


    def __init__(self, view, backup_path):
        """Settings can be accessed as attributes of this object.

        Attributes:
            file_path - The path to the original file that this object is used for
            backup_path - The backup path for this settings object
            _project_settings - The project settings at the time this object was created
            _settings_dict - A dictionary of settings compiled for the backup_path

        """
        super(BackupSettings, self).__init__()
        self.file_path = view.file_name()
        self.backup_path = backup_path
        self._project_settings = self.__class__.ProjectSettings(view)
        self._settings_dict = self._compile_settings_dict()
        self.__dict__.update(self._settings_dict)

    def _compile_settings_dict(self):
        """Builds a dictionary of settings from the defaults and backup path specific settings.

        Setting values are taken in the following order, from highest to lowest
        priority.
        1. Project level path-specific settings
        2. Package level path-specific settings
        3. Project level default settings
        4. Package level default settings
        5. Global default settings

        """
        settings_dict = BackupSettings.Defaults.copy()
        settings_dict.update(
            self.package_settings.get("default", dict())
            )

        if self.project_settings is not None:
            try:
                settings_dict.update(self.project_settings["default"])
            except KeyError:
                pass

        package_locations = self.package_settings.get('backup_locations', list())
        for b in package_locations:
            try:
                path = b['backup_path']
            except KeyError:
                continue

            if path == self.backup_path:
                d = b.copy()
                del d['backup_path']
                settings_dict.update(d)
                break

        if self.project_settings is not None:
            try:
                project_locations = self.project_settings['backup_locations']
            except KeyError:
                pass
            else:
                for b in project_locations:
                    try:
                        path = b['backup_path']
                    except KeyError:
                        continue

                    if path == self.backup_path:
                        d = b.copy()
                        del d['backup_path']
                        settings_dict.update(d)
                        break

        return settings_dict

    def parse_backup_increment(self, setting):
        """Return a tuple of (quantity, unit) for an increment setting.

        Keyword arguments:
        setting - the name of the setting to return

        Intended to be used with the "save_full_copy_every", "purge_after" and
        "archive_every" settings. If the setting can't be parsed, (0, '') is
        returned as a fallback setting so no files will be deleted.

        """
        try:
            value = getattr(self, setting)
        except AttributeError:
            logger.critical('Invalid setting: %s', setting)
            return (0, '')

        if isinstance(value, int):
            return (value, '')

        match = BackupSettings.BACKUP_INCREMENT_EXP.match(value)
        if match is None:
            logger.critical("Invalid increment setting value for %s: %s", setting, value)
            return (0, '')
        else:
            unit = match.group(2)
            if unit is None:
                unit = ''
            elif (unit[0] == 'd') or (unit[0] == 'D'):
                unit = 'daily'
            return (int(match.group(1)), unit)


class BackupFile(object):
    """Represents a file saved for backup."""


    FILENAME_EXP = re.compile(r"([\d. ]+) v(\d+)( \(A\))?( - (.+?))?\.[^.]")
    FILENAME_FORMAT = '{datetime} v{version}{archive}{name}{ext}'
    DIFF_EXTENSION = '.incdiff'
    BackupTime = time.localtime()
    DMP = diff_match_patch()


    @classmethod
    def update_backup_time(cls):
        """Updates the stored backup time to the current time.

        This time is cached so that all backups across multiple locations have
        the same time. This time is used for uniqueness since they may have
        different version numbers.

        """
        cls.BackupTime = time.localtime()

    @classmethod
    def build_backup_file_name(cls, version, ext, name = '', archive = False, datetime = None):
        """Generate and return the name of a backup file based on the arguments."""
        if datetime is None:
            datetime = time.strftime(FILE_TIME_FORMAT, BackupFile.BackupTime)
        else:
            datetime = time.strftime(FILE_TIME_FORMAT, datetime)
        if name:
            name = ' - ' + name
        else:
            name = ''
        if archive:
            archive = " (A)"
        else:
            archive = ''
        return BackupFile.FILENAME_FORMAT.format(datetime = datetime,
                                                 version = version,
                                                 name = name,
                                                 ext = ext,
                                                 archive = archive)

    @classmethod
    def parse_file_name(cls, file_name):
        """Parse the filename, extracting metadata about the backup."""
        match = BackupFile.FILENAME_EXP.match(file_name)
        if match is None:
            raise ValueError("Filename could not be parsed: %s" % file_name)
        datetime = time.strptime(match.group(1), FILE_TIME_FORMAT)
        version = int(match.group(2))
        if match.group(3) is None:
            is_archive = False
        else:
            is_archive = True
        if match.group(4) is None:
            name = ''
        else:
            name = match.group(5)
        return (datetime, version, name, is_archive)


    @property
    def backup_file_path(self):
        return self._backup_file_path

    @backup_file_path.setter
    def backup_file_path(self, value):
        self._backup_file_path = value
        self.backup_file_folder, self.backup_file_name = os.path.split(self._backup_file_path)

    @property
    def backup_folder(self):
        try:
            return self.backup_handler.backup_path
        except AttributeError:
            return None

    @property
    def original_file_path(self):
        try:
            return self.backup_handler.file_path
        except AttributeError:
            return None

    @property
    def ext(self):
        """Returns the extension of the backup_file."""
        return os.path.splitext(self.backup_file_path)[1]

    @property
    def datetime(self):
        return self._datetime

    @datetime.setter
    def datetime(self, value):
        self._datetime = value
        t = time.mktime(self.datetime)
        self.date_object = date.fromtimestamp(t)

    @property
    def is_diff(self):
        return self.ext == BackupFile.DIFF_EXTENSION

    @property
    def file_contents(self):
        if self._file_contents is None:
            self._file_contents = self.get_file_contents()
        return self._file_contents

    @property
    def settings(self):
        return self.backup_handler.settings


    def __init__(self, backup_file_path, backup_handler):
        """
        Attributes:
            backup_handler
            backup_file_path
            backup_file_name
            backup_file_folder
            _datetime
            dateobject
            version
            version_name
            is_archive
            original_ext
            _file_contents

        """
        super(BackupFile, self).__init__()
        self.backup_handler = backup_handler
        self.backup_file_path = backup_file_path
        self.datetime, self.version, self.version_name, self.is_archive = BackupFile.parse_file_name(self.backup_file_name)
        self.original_ext = os.path.splitext(self.original_file_path)[1]
        self._file_contents = None


    def __lt__(self, other):
        """Return True if this class is less than other.

        The class name is used to compare. If other has a __repr__ method, it
        is used for comparison. Otherwise, __name__ is used.

        """
        if isinstance(other, BackupFile):
            if self.datetime == other.datetime:
                return self.version < other.version
            else:
                return self.datetime < other.datetime
        else:
            return super(BackupFile, self).__lt__(other)

    def __str__(self):
        return self.backup_file_path

    def patch_make(self, base_contents):
        """Creates a patch set by comparing this file to base_contents."""
        with open(self.backup_file_path, 'r') as current:
            patches = BackupFile.DMP.patch_make(base_contents, current.read())
        return BackupFile.DMP.patch_toText(patches)

    def patch_apply(self, base_contents):
        """Returns the contents obtained by applying this file as a patch to base_contents."""
        with open(self.backup_file_path, 'r') as current:
            changes = current.read()
        # logger.debug('changes = %s', changes)
        patches = BackupFile.DMP.patch_fromText(changes)
        contents, errors = BackupFile.DMP.patch_apply(patches, base_contents)
        # logger.debug('errors = %s', errors)
        return contents

    def convert_to_diff(self, base_contents):
        """Converts the BackupFile from a full file to a diff."""
        if self.is_diff:
            return

        text = self.patch_make(base_contents)
        file_path = os.path.splitext(self.backup_file_path)[0] + BackupFile.DIFF_EXTENSION
        try:
            with open(file_path, 'w') as diff:
                diff.write(text)
        except OSError:
            logger.warning('Unable to convert file to diff: %s', self.backup_file_path)
        else:
            os.remove(self.backup_file_path)
            self.backup_file_path = file_path

    def convert_from_diff(self):
        """Converts the BackupFile from a diff to a full file."""
        if not self.is_diff:
            return

        text = self.get_file_contents()
        file_path = os.path.splitext(self.backup_file_path)[0] + self.original_ext
        try:
            with open(file_path, 'w') as diff:
                diff.write(text)
        except OSError:
            logger.warning('Unable to convert file from diff: %s', file_path)
        else:
            os.remove(self.backup_file_path)
            self.backup_file_path = file_path

    def get_file_contents(self, split_lines = False):
        """Return the contents of the file.

        Keyword arguments:
        file_list - A list of BackupFile objects for the current backup location.

        If this is an incdiff file, the file_list is searched for the current
        file. When it is found, it and any following incdiff files are patched
        onto the next newer version of the full file to get the file contents
        of this version.

        """
        if self._file_contents is None:
            if not self.is_diff:
                with open(self.backup_file_path, 'r') as f:
                    self._file_contents = f.read()
            else:
                # Archives are always stored as full files, so I don't need
                # archives in the file list
                file_list = self.backup_handler.get_file_list()
                diffs = [self]
                i = file_list.index(self)
                i -= 1
                try:
                    while file_list[i].is_diff and (file_list[i]._file_contents is None):
                        diffs.append(file_list[i])
                        i -= 1
                except IndexError:
                    logger.error('No base file to apply diffs. Please save %s', self.original_file_path)

                base = file_list[i]
                base_contents = base.get_file_contents()
                for d in reversed(diffs):
                    logger.debug("Applying patch %s", d.backup_file_name)
                    base_contents = d.patch_apply(base_contents)
                    d._file_contents = base_contents

        if split_lines:
            return split_string_into_lines(self._file_contents)
        else:
            return self._file_contents

    def get_diff_contents(self, to_contents, to_time):
        """Return a unified diff of the current contents and to_contents.

        This is a reverse diff, so the newer contents (to_contents) are used
        as the base.

        """
        from_contents = self.get_file_contents(split_lines = True)
        if isinstance(to_contents, str):
            to_contents = split_string_into_lines(to_contents)
        from_time = time.asctime(self.datetime)
        to_time = time.asctime(to_time)
        # Since this is a reverse diff, the arguments are supplied in the reverse order.
        diff_contents = difflib.unified_diff(to_contents,
                                             from_contents,
                                             fromfile = self.original_file_path,
                                             tofile = self.original_file_path,
                                             fromfiledate = to_time,
                                             tofiledate = from_time)
        return ''.join([l for l in diff_contents])

    def quick_panel_contents(self):
        """Return the contents to display for the file in the quick panel."""
        if self.is_archive:
            archive = '  (Archived)'
        else:
            archive = ''
        if self.version_name:
            name = self.version_name
        else:
            name = 'version %s' % self.version
        return ['{backup_date} - {name:<20}{archive}'.format(
                    backup_date = time.strftime(DISPLAY_TIME_FORMAT, self.datetime),
                    name = name,
                    archive = archive),
                self.backup_folder]

    def opened_view_name(self):
        """Return a view name to use for the opened file."""
        if self.version_name:
            name = self.version_name
        else:
            name = 'version %s' % self.version
        return '{original_file_name} - {backup_date} - {name}'.format(
            original_file_name = os.path.basename(self.original_file_path),
            backup_date = time.strftime(DISPLAY_TIME_FORMAT, self.datetime),
            name = name)

    def archive(self):
        """Archives this file.

        A file is converted from a diff if necessary and then renamed or
        moved to the appropriate archive path.

        """
        logger.debug('Archiving file %s', self.backup_file_path)
        if self.is_diff:
            self.convert_from_diff()
        new_name = BackupFile.build_backup_file_name(self.version,
                                                     self.ext,
                                                     self.version_name,
                                                     archive = True,
                                                     datetime = self.datetime)
        if self.settings.use_archive_folder:
            target_path = os.path.join(self.backup_file_folder, 'Archive', new_name)
        else:
            target_path = os.path.join(self.backup_file_folder, new_name)
        try:
            shutil.move(self.backup_file_path, target_path)
            self.backup_file_path = target_path
            self.is_archive = True
        except OSError:
            logger.warning('Failed to archive file %s', self.backup_file_path)

    def purge(self):
        """Deletes this file."""
        logger.debug('Purging file %s', self.backup_file_path)
        try:
            os.remove(self.backup_file_path)
        except OSError:
            logger.warning('Failed to purge file %s', self.backup_file_path)


